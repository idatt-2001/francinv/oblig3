package no.ntnu.idatx2001.oblig3;

import javafx.application.Application;

public class CardApplication {
    public static void main(String[] args) {
        Application.launch(CardUi.class, args);
    }
}
