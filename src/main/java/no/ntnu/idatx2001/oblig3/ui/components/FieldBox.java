package no.ntnu.idatx2001.oblig3.ui.components;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class FieldBox extends HBox {
    public FieldBox(FieldLabel label, ValueField input) {
        this.setPadding(new Insets(10, 5, 5, 10));
        this.setSpacing(12);
        this.setStyle(
            "-fx-border-radius: 12px;" +
            "-fx-background-radius: 12px;" +
            "-fx-border-color: #01012f;" +
            "-fx-border-width: 1px;"
        );
        this.getChildren().addAll(label, input);

        // Align input to right
        HBox.setHgrow(input, Priority.ALWAYS);
    }
}
