package no.ntnu.idatx2001.oblig3.ui.components;

import javafx.scene.control.Label;

public class FieldLabel extends Label {
    public FieldLabel(String text) {
        super(text);
        this.setStyle(
            "-fx-font-family: Avenir;" +
            "-fx-font-size: 18px;" +
            "-fx-font-weight: bold;"
        );
    }
}
