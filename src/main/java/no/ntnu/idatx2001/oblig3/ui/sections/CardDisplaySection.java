package no.ntnu.idatx2001.oblig3.ui.sections;

import javafx.geometry.Insets;
import javafx.scene.control.ListView;
import javafx.scene.layout.*;
import no.ntnu.idatx2001.oblig3.ui.components.LeftRoundedButton;
import no.ntnu.idatx2001.oblig3.ui.components.RightRoundedButton;
import no.ntnu.idatx2001.oblig3.ui.components.Title;


public class CardDisplaySection extends GridPane {
    public CardDisplaySection(LeftRoundedButton leftRoundedButton, RightRoundedButton rightRoundedButton, ListView cardView) {
        this.setPadding(new Insets(12, 12, 12, 12));
        this.setPrefSize(800, 250);
        this.setVgap(12);
        this.setHgap(75);

        //Title
        Title sectionTitle = new Title("Your cards");

        //Buttons

        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(leftRoundedButton, rightRoundedButton);

        this.add(sectionTitle, 0, 0, 1, 1);
        this.add(buttonBox, 1, 0, 1, 1);
        this.add(cardView, 0, 2, 3, 1);
    }
}
