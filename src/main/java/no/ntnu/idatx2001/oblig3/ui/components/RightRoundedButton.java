package no.ntnu.idatx2001.oblig3.ui.components;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class RightRoundedButton extends Button {
    public RightRoundedButton(String text, EventHandler<ActionEvent> onAction) {
        super(text);
        this.setPrefSize(150, 40);
        this.setStyle(
            "-fx-background-color: #01012F;" +
            "-fx-background-radius: 0 12px 12px 0;" +
            "-fx-border-radius: 0 12px 12px 0;" +
            "-fx-text-fill: #ffffff;" +
            "-fx-font-family: Avenir;" +
            "-fx-font-size: 18px;" +
            "-fx-font-weight: 500;" +
                // Border on left side
                "-fx-border-color: #ffffff;" +
                "-fx-border-width: 0 0 0 1px;"
        );
        this.setOnAction(onAction);
    }
}
