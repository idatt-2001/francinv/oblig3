package no.ntnu.idatx2001.oblig3.ui.components;


import javafx.scene.control.TextField;

public class ValueField extends TextField {
    public ValueField(String placeholder, String value) {
        this.setPrefSize(150, 30);
        this.setPromptText(placeholder);
        this.setText(value);

        this.setStyle(
            "-fx-background-color: #E6EFF2;" +
            "-fx-border-radius: 12px;" +
            "-fx-background-radius: 12px;" +
            "-fx-padding: 5px;" +
            "-fx-font-family: Avenir;" +
            "-fx-font-size: 14px;"
        );

    }
}
