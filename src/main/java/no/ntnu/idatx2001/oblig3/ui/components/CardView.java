package no.ntnu.idatx2001.oblig3.ui.components;

import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import no.ntnu.idatx2001.oblig3.cardgame.PlayingCard;

public class CardView extends ListView<PlayingCard> {
    public CardView(ObservableList<PlayingCard> items) {
        this.setPrefSize(800, 200);
        this.setStyle(
            "-fx-background-radius: 12px;" +
            "-fx-border-radius: 12px;" +
            "-fx-padding: 12px;" +
            "-fx-background-color: rgba(47, 122, 146, 0.4);"
        );
        this.setItems(items);
    }
}
