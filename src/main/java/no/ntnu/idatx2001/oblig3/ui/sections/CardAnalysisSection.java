package no.ntnu.idatx2001.oblig3.ui.sections;

import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import no.ntnu.idatx2001.oblig3.ui.components.FieldBox;
import no.ntnu.idatx2001.oblig3.ui.components.FieldLabel;
import no.ntnu.idatx2001.oblig3.ui.components.Title;
import no.ntnu.idatx2001.oblig3.ui.components.ValueField;

public class CardAnalysisSection extends GridPane {
    public CardAnalysisSection(ValueField faceField, ValueField isFlush, ValueField heartCardsField, ValueField hasQueenOfSpadesField) {
        this.setPadding(new Insets(12, 12, 12, 12));
        this.setPrefSize(800, 500);
        this.setHgap(12);
        this.setVgap(12);

        Title sectionTitle = new Title("Analysis of your cards");

        this.add(sectionTitle, 1, 0, 2, 1);

        FieldBox faceBox = new FieldBox(
            new FieldLabel("Sum of the faces:"),
            faceField
        );
        FieldBox isFlushBox = new FieldBox(
            new FieldLabel("Flush:"),
            isFlush
        );
        FieldBox heartCardsBox = new FieldBox(
            new FieldLabel("Cards of hearts:"),
            heartCardsField
        );
        FieldBox hasQueenOfSpadesBox = new FieldBox(
            new FieldLabel("Queen of spades:"),
            hasQueenOfSpadesField
        );

        this.add(faceBox, 1, 1);
        this.add(isFlushBox, 2, 1);
        this.add(heartCardsBox, 1, 2);
        this.add(hasQueenOfSpadesBox, 2, 2);
    }
}
