package no.ntnu.idatx2001.oblig3.ui.controllers;

import javafx.collections.ObservableList;
import no.ntnu.idatx2001.oblig3.cardgame.PlayerHand;
import no.ntnu.idatx2001.oblig3.cardgame.PlayingCard;

public class CardController {
    private final PlayerHand playerHand;
    private int handValue;
    private String heartsAsString;
    private boolean queenOfSpades;
    private boolean fiveFlush;

    public CardController() {
        this.playerHand = new PlayerHand();
    }

    public void dealHand(int n) {
        this.playerHand.dealHand(n);
    }

    public void checkHand() {
        this.handValue = this.playerHand.calculateHandValue();
        this.heartsAsString = this.playerHand.getHeartsAsString();
        this.queenOfSpades = this.playerHand.checkIfHandContainsQueenOfSpades();
        this.fiveFlush = this.playerHand.checkIfFiveFlush();
    }

    public int getHandValue() {
        return this.handValue;
    }

    public String getHeartsAsString() {
        return this.heartsAsString;
    }

    public boolean getQueenOfSpades() {
        return this.queenOfSpades;
    }

    public boolean getFiveFlush() {
        return this.fiveFlush;
    }

    public ObservableList<PlayingCard> getHand() {
        return this.playerHand.getHand();
    }

}
