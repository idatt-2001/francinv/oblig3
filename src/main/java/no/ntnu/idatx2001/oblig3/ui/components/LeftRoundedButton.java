package no.ntnu.idatx2001.oblig3.ui.components;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class LeftRoundedButton extends Button {
    public LeftRoundedButton(String text, EventHandler<ActionEvent> onAction) {
        super(text);
        this.setPrefSize(150, 40);
        this.setStyle(
            "-fx-background-color: #01012F;" +
            "-fx-background-radius: 12px 0 0 12px;" +
            "-fx-border-radius: 12px 0 0 12px;" +
            "-fx-text-fill: #ffffff;" +
            "-fx-font-family: Avenir;" +
            "-fx-font-size: 18px;" +
            "-fx-font-weight: bolder;"
        );
        this.setOnAction(onAction);
    }
}
