package no.ntnu.idatx2001.oblig3.ui.components;

import javafx.scene.control.Label;

public class Title extends Label {
    public Title(String text) {
        super(text);
        this.setStyle(
            "-fx-font-size: 30px;" +
            "-fx-font-weight: bolder;" +
            "-fx-text-fill: #01012f;" +
            "-fx-font-family: Avenir;"
        );
    }
}
