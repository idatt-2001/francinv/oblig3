package no.ntnu.idatx2001.oblig3.cardgame;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PlayerHand {
    private ObservableList<PlayingCard> hand;
    private DeckOfCards deckOfCards;

    public PlayerHand() {
        this.hand = FXCollections.observableArrayList();
        this.deckOfCards = new DeckOfCards();
    }

    public ObservableList<PlayingCard> getHand() {
        return this.hand;
    }

    public void dealHand(int n) {
        this.hand = this.deckOfCards.dealHand(n);
    }

    /**
     * The method returns the value of the hand.
     *
     * @return the value of the hand
     */
    public int calculateHandValue() {
        return this.getHand().stream().mapToInt(PlayingCard::getFace).sum();
    }

    /**
     * The method returns the hearts in the hand as a string.
     *
     * @return the hearts in the hand as a string
     */
    public String getHeartsAsString() {
        return this.getHand().stream().filter(card -> card.getSuit() == 'H').map(PlayingCard::getAsString)
                .reduce("", (a, b) -> a + b + " ");
    }

    /**
     * The method checks if the hand contains the queen of spades.
     *
     * @return true if the hand contains the queen of spades, false otherwise
     */
    public boolean checkIfHandContainsQueenOfSpades() {
        return this.getHand().stream().anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    }

    /**
     * The method checks if the hand contains a 5-flush.
     *
     * @return true if the hand contains a 5-flush, false otherwise
     */
    public boolean checkIfFiveFlush() {
        return this.getHand().stream().filter(card -> card.getSuit() == 'H').count() == 5 ||
                this.getHand().stream().filter(card -> card.getSuit() == 'S').count() == 5 ||
                this.getHand().stream().filter(card -> card.getSuit() == 'D').count() == 5 ||
                this.getHand().stream().filter(card -> card.getSuit() == 'C').count() == 5;
    }


}
