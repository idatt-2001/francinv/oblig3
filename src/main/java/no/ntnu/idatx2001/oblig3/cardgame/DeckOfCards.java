package no.ntnu.idatx2001.oblig3.cardgame;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;

/**
 * The class represents a deck of cards.
 *
 * @author francinv
 * @version 2023-03-07
 */
public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private ArrayList<PlayingCard> cardDeck;

    public DeckOfCards() {
        this.cardDeck = generateDeckOfCards();
    }

    /**
     * The method creates all the cards and returns a list of them.
     *
     * @return a deck of cards
     */
    private ArrayList<PlayingCard> generateDeckOfCards() {
        ArrayList<PlayingCard> deck = new ArrayList<>();
        for (char c : suit) {
            for (int i = 1; i <= 13; i++) {
                deck.add(new PlayingCard(c, i));
            }
        }
        return deck;
    }

    /**
     * The method returns the deck of cards.
     *
     * @return the deck of cards
     */
    public ArrayList<PlayingCard> getCardDeck() {
        return this.cardDeck;
    }

    /**
     * The methods picks a random number of cards from the deck and returns them.
     *
     * @param n the number of cards to be picked
     * @returns a list of cards
     */
    public ObservableList<PlayingCard> dealHand(int n) {
        ObservableList<PlayingCard> hand = FXCollections.observableArrayList();
        for (int i = 0; i < n; i++) {
            int random = (int) (Math.random() * this.cardDeck.size());
            hand.add(this.cardDeck.get(random));
            this.cardDeck.remove(random);
        }
        return hand;
    }
}
