package no.ntnu.idatx2001.oblig3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import no.ntnu.idatx2001.oblig3.cardgame.PlayingCard;
import no.ntnu.idatx2001.oblig3.ui.components.LeftRoundedButton;
import no.ntnu.idatx2001.oblig3.ui.components.RightRoundedButton;
import no.ntnu.idatx2001.oblig3.ui.components.ValueField;
import no.ntnu.idatx2001.oblig3.ui.controllers.CardController;
import no.ntnu.idatx2001.oblig3.ui.sections.CardAnalysisSection;
import no.ntnu.idatx2001.oblig3.ui.sections.CardDisplaySection;


public class CardUi extends Application {

        @Override
        public void start(Stage primaryStage) throws Exception {
            primaryStage.setTitle("Card Application");
            CardController cardController = new CardController();

            //Card display
            ListView<PlayingCard> cardView = new ListView(cardController.getHand());
            cardView.setStyle(
                    "-fx-font-family: Avenir; " +
                    "-fx-font-size: 16px; " +
                    "-fx-border-radius: 12px; " +
                    "-fx-background-radius: 12px; " +
                    "-fx-background-color: rgba(47, 122, 146, 0.4); " +
                    "-fx-padding: 12px;"
            );

            // Fields
            ValueField faceField = new ValueField(
                    "Face value",
                    ""
            );
            ValueField isFlush = new ValueField(
                    "Yes/No",
                    ""
            );
            ValueField heartCardsField = new ValueField(
                    "Your heart cards",
                    ""
            );
            ValueField hasQueenOfSpades = new ValueField(
                    "Yes/No",
                    ""
            );

            LeftRoundedButton leftRoundedButton = new LeftRoundedButton("Deal Hand", actionEvent -> {
                cardController.dealHand(5);
                cardView.setItems(cardController.getHand());
            });
            RightRoundedButton rightRoundedButton = new RightRoundedButton("Check Hand", actionEvent -> {
                cardController.checkHand();
                faceField.setText(String.valueOf(cardController.getHandValue()));
                isFlush.setText(cardController.getFiveFlush() ? "Yes" : "No");
                heartCardsField.setText(cardController.getHeartsAsString());
                hasQueenOfSpades.setText(cardController.getQueenOfSpades() ? "Yes" : "No");
            });


            CardDisplaySection cardDisplaySection = new CardDisplaySection(leftRoundedButton, rightRoundedButton, cardView);
            CardAnalysisSection cardAnalysisSection = new CardAnalysisSection(faceField, isFlush, heartCardsField, hasQueenOfSpades);

            GridPane gridPane = new GridPane();
            gridPane.add(cardDisplaySection, 0, 0);
            gridPane.add(cardAnalysisSection, 0, 1);

            BorderPane layout = new BorderPane();
            layout.setPrefSize(1000, 1000);
            layout.setCenter(gridPane);

            Scene scene = new Scene(layout, 1000, 1000);
            primaryStage.setScene(scene);
            primaryStage.show();
        }

        public static void main(String[] args) {
            launch(args);
        }
}
